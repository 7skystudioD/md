﻿#pragma strict
public var butArr : button[];
public var goNoActive : GameObject;
public var goActive : GameObject;
function Start () 
{
	while(butArr.Length>0)
	{
		var active : boolean = true;
		for(var cur_but : button in butArr)
		{
			if(!cur_but.isActive)
				active = false;
		}
		if(active)
		{
			if(goActive)
				goActive.SetActive(true);
			if(goNoActive)
				goNoActive.SetActive(false);
			
		}
		else
		{
			if(goActive)
				goActive.SetActive(false);
			if(goNoActive)
				goNoActive.SetActive(true);
		}
		yield WaitForSeconds(0.1);
	}
}

function Update () {

}