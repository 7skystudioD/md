﻿#pragma strict
public var relay : boolean = true;
public var sendStart : GameObject[];
function Start () 
{
	if(!relay)
		return;
	for(var go : GameObject in sendStart)
		go.SendMessage("Start");
}
function OnEnable()
{
	if(!relay)
		return;
	Start();
}
function OnAwake () 
{
	if(!relay)
		return;
	Start();
}