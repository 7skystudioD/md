#pragma strict
var endPos : Vector2;
var loop : boolean;
public var takeParent : boolean;
public var StartOnEnter : boolean;
public var rigid : Rigidbody2D;
private var ingo : Transform;
function Atan2(x : float , y : float):float
{
	if(x == 0 && y == 0)
		return 0;
	var a = Mathf.Acos(x / Mathf.Sqrt(x*x + y*y));
	return y >= 0 ? a : -a;
}
function setSpeed(velX : float, velY : float, part : float)
{
	rigid.velocity.x = velX*part;
	rigid.velocity.y = velY*part;
	yield WaitForSeconds(0.1);
}
function Start () 
{
	var begin_pos : Vector2 = transform.position;
	var iter : float=Vector2.Distance(begin_pos,endPos);
	var angle = Mathf.Round(Atan2((endPos.x-begin_pos.x),(endPos.y-begin_pos.y))*180/3.14);
	
	yield WaitForSeconds(0.1);
	while(StartOnEnter)
		yield WaitForSeconds(0.1);
	
	while(loop)
	{
		var velX = Mathf.Cos(angle*3.14/180) * 5;
		var velY = Mathf.Sin(angle*3.14/180) * 5;
		setSpeed(velX, velY, 1);
		while(Vector2.Distance(transform.position,endPos)>0.1)
		//for(var i=0f;i<(iter*60);i+=5)
			yield WaitForSeconds(0.001);
		//rigid.velocity.y = 0;
		setSpeed(velX, velY, 0);
		yield WaitForSeconds(3);
		
		velX *=-1;velY *=-1;
		setSpeed(velX, velY, 1);
		while(Vector2.Distance(transform.position,begin_pos)>0.1)
		//for(var i=0f;i<(iter*60);i+=5)
			yield WaitForSeconds(0.001);
		//rigid.velocity.y = 0;
		setSpeed(velX, velY, 0);
		yield WaitForSeconds(3);
		
		/*
		
		for(var i=0f;i<(iter*10);i+=0.4)
		{
			//transform.position = Vector2.Lerp(begin_pos, endPos, i/(iter*10));
			if(ingo != null)
			{
				ingo.position.y = transform.position.y+2.27;
				ingo.rotation.eulerAngles.z = 0;
			}
			rigid.velocity.y = -5;
			//rigid.position=(Vector2.Lerp(begin_pos, endPos, i/(iter*10)));
		  	yield WaitForSeconds(0.001/(iter*10000));
		}*/
		/*
		yield WaitForSeconds(2);
		for(i=0f;i<(iter*10);i+=0.4)
		{
			rigid.velocity.y = 5;
			//transform.position = Vector2.Lerp(endPos, begin_pos, i/(iter*10));
			//rigid.position=(Vector2.Lerp(endPos, begin_pos, i/(iter*10)));
			if(ingo != null)
			{
				ingo.position.y = transform.position.y+2.27;
				ingo.rotation.eulerAngles.z = 0;
			}
		  	yield WaitForSeconds(0.001/(iter*10000));
		}
		yield WaitForSeconds(2);*/
	}
	//Debug.Log(endPos);
	for(var q=0f;q<(iter*10);q+=0.4)
	{
		rigid.position = Vector2.Lerp(begin_pos, endPos, q/(iter*10));
		if(ingo != null)
			ingo.rotation.eulerAngles.z = 0;
		//Debug.Log(endPos);
	  	yield WaitForSeconds(0.01);
	}
	
}

function OnTriggerEnter2D (go : Collider2D) 
{
	if(go.gameObject.tag == gameObject.tag)
	{
		yield WaitForSeconds(0.8);
		StartOnEnter = false;
	}
	Debug.Log("tooked "+go.gameObject);
	if(!takeParent || go.gameObject.tag != gameObject.tag)
		return;
	go.transform.position.x = transform.position.x;
	Debug.Log("tooked "+go.gameObject);
	go.attachedRigidbody.freezeRotation = true;
	go.transform.rotation.eulerAngles.z = 0;
	if(go.transform.parent !=null && go.transform.parent.gameObject.tag !="Level")
		go.transform.parent.parent = gameObject.transform;	
	else
		go.transform.parent = transform;
	ingo = go.transform;
}
function OnTriggerExit2D (go : Collider2D) 
{
	if(!takeParent || go.gameObject.tag != gameObject.tag)
		return;
	Debug.Log("dropped "+go.gameObject);	
	go.attachedRigidbody.freezeRotation = false;
	if(go.transform.parent !=null)// && go.transform.parent.gameObject.tag !="Level")
		go.transform.parent.parent = null;
	else
		go.transform.parent = null;
	ingo = null;
}