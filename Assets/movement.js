#pragma strict
public var now_item : Transform;
public var item_to_take : Transform;
public var arm_pos : Transform;
public var rigid : Rigidbody2D;
public var joy : CNJoystick;
function use()
{
	if(now_item != null)
		now_item.BroadcastMessage("use");
}
function TakeDrop()
{
	drop();
	take_item();
}
function Start () 
{
	
	while(true)
	{
		Camera.main.transform.position.x = transform.position.x;
		Camera.main.transform.position.y = transform.position.y-1;
		if(transform.rotation.eulerAngles.z >=10 && transform.rotation.eulerAngles.z <=350)
		{
			yield WaitForSeconds(0.2);
			transform.rotation.eulerAngles.z = 0;
		}
		yield WaitForSeconds(0.01);
	}
}
function moveRight()
{
	if(global.move)
	{
		if(transform.localScale.x < 0)
		{
			if(now_item !=null)
				now_item.localScale.x = -now_item.localScale.x; 
			transform.localScale.x = -transform.localScale.x;
		}
		//rigid.velocity.x = 10;
		rigid.AddRelativeForce(Vector2(100,0));
		//if(rigid.velocity.x > 10)
			//rigid.velocity.x = 10;
	}		
}
function moveLeft()
{
	if(global.move)
	{
		if(transform.localScale.x > 0)
		{
			if(now_item !=null)
				now_item.localScale.x = -now_item.localScale.x; 
			transform.localScale.x = -transform.localScale.x;
		}
		//rigid.velocity.x = -10;
		rigid.AddRelativeForce(Vector2(-100,0));
		//if(rigid.velocity.x < 10)
			//rigid.velocity.x = -10;
	}
}
function Update () 
{
	//return;
	if((Input.GetAxis("Horizontal")>0.01 || joy.GetAxis("H")>0) && global.move)//right
	{
		moveRight();
	}
	else if((Input.GetAxis("Horizontal")<(-0.01) || joy.GetAxis("H")<0) && global.move)
	{
		moveLeft();
	}
	else
		rigid.velocity.x = 0;
	
	if(now_item !=null)
		now_item.position = arm_pos.position;
	if(transform.localScale.x != 2 && transform.localScale.x != -2)
		transform.localScale.x = 2;
	if(transform.localScale.y != 2)
		transform.localScale.y = 2;
	if(transform.rotation.eulerAngles.z == 180)
		transform.rotation.eulerAngles.z = 0;
}
function OnTriggerEnter2D (go : Collider2D)
{
	if(go.name.Contains("emirald"))
	{
		global.imeralds++;
		go.gameObject.SetActive(false);
		return;
	}
	if((go.gameObject.tag=="arm_item" || go.gameObject.tag=="button_able") && item_to_take != go.transform && now_item != go.transform && !go.name.Contains("lift"))
		item_to_take = go.transform;
}
function OnTriggerExit2D (go : Collider2D)
{
	if(go.gameObject.tag=="arm_item" || go.gameObject.tag=="button_able")
		item_to_take = null;
}
function drop()
{
	if(now_item !=null)
	{	
		
		now_item.GetComponent.<Collider2D>().enabled = true;
		now_item.GetComponent.<Rigidbody2D>().freezeRotation = false;
		now_item.transform.position.x+=1;
		now_item.transform.position.y+=2;
		now_item = null;
	}
	now_item = null;
}
function take_item()
{
	if(item_to_take == null)
		return;
	if(now_item !=null)
	{
		now_item.GetComponent.<Collider2D>().enabled = true;
		now_item.GetComponent.<Rigidbody2D>().freezeRotation = false;
		//now_item.transform.position.x+=1;
		//now_item.transform.position.y+=2;
		now_item = null;
	}
	now_item = item_to_take.transform;
	if((transform.localScale.x < 0 && now_item.localScale.x > 0) || (transform.localScale.x > 0 && now_item.localScale.x < 0))
		transform.localScale.x = -transform.localScale.x;
	now_item.GetComponent.<Rigidbody2D>().isKinematic = true;
	now_item.transform.rotation.eulerAngles.z=0;
	now_item.GetComponent.<Rigidbody2D>().isKinematic = false;
	global.item_in_use = now_item.gameObject;
	now_item.GetComponent.<Collider2D>().enabled = false;
	now_item.GetComponent.<Rigidbody2D>().freezeRotation = true;
	item_to_take = null;
	
} 