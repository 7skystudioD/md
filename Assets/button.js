﻿#pragma strict
public var isActive : boolean;
public var anyObject : boolean;
public var on : Sprite;
public var off : Sprite;
private var rend : SpriteRenderer;
private var entered : boolean;
function Start () 
{
	rend = gameObject.GetComponent.<SpriteRenderer>();
	while(true)
	{
		if(entered)
		{
			yield WaitForSeconds(0.1);
			if(entered)
			{
				//Debug.Log(go.gameObject);
				isActive = true;
				if(rend.sprite)
					rend.sprite = on;
			}
		}
		yield WaitForSeconds(0.01);
	}
}

//function OnCollisionEnter2D (go : Collision2D)
function OnTriggerEnter2D (go : Collider2D)
{
	if(go.gameObject.tag == "button_able" || go.gameObject.tag == "basket_items" || anyObject)
	{
		entered = true;
	}
}
//function OnCollisionExit2D (go : Collision2D)
function OnTriggerExit2D (go : Collider2D)
{
	if(go.gameObject.tag == "button_able"|| go.gameObject.tag == "basket_items" || anyObject)
	{
		entered = false;
		isActive = false;
		if(rend.sprite)
			rend.sprite = off;
	}
}